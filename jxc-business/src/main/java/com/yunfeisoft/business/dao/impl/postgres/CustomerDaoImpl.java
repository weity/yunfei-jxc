package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.CustomerDao;
import com.yunfeisoft.business.model.Customer;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * ClassName: CustomerDaoImpl
 * Description: 客户信息Dao实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Repository
public class CustomerDaoImpl extends ServiceDaoImpl<Customer, String> implements CustomerDao {

    @Override
    public Page<Customer> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithDesc("createTime");
        wb.addOrderByWithDesc("code");
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("orgId", params.get("orgId"));
            wb.andFullLike("name", params.get("name"));
        }
        return queryPage(wb);
    }

    @Override
    public List<Customer> queryList(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        if (params != null) {
            wb.andEquals("orgId", params.get("orgId"));
            wb.andFullLike("name", params.get("name"));
        }
        return query(wb);
    }

    @Override
    public boolean isDupName(String orgId, String id, String name) {
        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("isDel", 2);
        wb.andEquals("orgId", orgId);
        wb.andEquals("name", name);
        wb.andNotEquals("id", id);
        return this.count(wb) > 0;
    }
}