package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.ProductCategoryDao;
import com.yunfeisoft.business.model.ProductCategory;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * ClassName: ProductCategoryDaoImpl
 * Description: 商品类别Dao实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Repository
public class ProductCategoryDaoImpl extends ServiceDaoImpl<ProductCategory, String> implements ProductCategoryDao {

    @Override
    public Page<ProductCategory> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithDesc("createTime");
        wb.addOrderByWithDesc("code");
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("orgId", params.get("orgId"));
            wb.andEquals("parentId", params.get("parentId"));
            wb.andFullLike("idPath", params.get("idPath"));
            wb.andFullLike("name", params.get("name"));
        }
        return queryPage(wb);
    }

    @Override
    public List<ProductCategory> queryList(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithDesc("createTime");
        wb.addOrderByWithAsc("code");
        if (params != null) {
            wb.andEquals("orgId", params.get("orgId"));
            wb.andFullLike("idPath", params.get("idPath"));
            wb.andFullLike("name", params.get("name"));
        }
        return query(wb);
    }

    @Override
    public List<ProductCategory> queryByName(String orgId, String name) {
        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("orgId", orgId);
        wb.andEquals("name", name);
        return query(wb);
    }
}