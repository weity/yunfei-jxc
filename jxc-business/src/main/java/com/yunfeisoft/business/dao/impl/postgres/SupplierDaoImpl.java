package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.SupplierDao;
import com.yunfeisoft.business.model.Supplier;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * ClassName: SupplierDaoImpl
 * Description: 供应商信息Dao实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Repository
public class SupplierDaoImpl extends ServiceDaoImpl<Supplier, String> implements SupplierDao {

    @Override
    public Page<Supplier> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithDesc("createTime");
        wb.addOrderByWithDesc("code");
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("orgId", params.get("orgId"));
            wb.andFullLike("name", params.get("name"));
        }
        return queryPage(wb);
    }

    @Override
    public List<Supplier> queryList(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        if (params != null) {
            wb.andEquals("orgId", params.get("orgId"));
            wb.andFullLike("name", params.get("name"));
        }
        return query(wb);
    }

    @Override
    public List<Supplier> queryByName(String orgId, String name) {
        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("orgId", orgId);
        wb.andEquals("name", name);
        return query(wb);
    }

    @Override
    public boolean isDupName(String orgId, String id, String name) {
        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("isDel", 2);
        wb.andEquals("orgId", orgId);
        wb.andEquals("name", name);
        wb.andNotEquals("id", id);
        return this.count(wb) > 0;
    }
}