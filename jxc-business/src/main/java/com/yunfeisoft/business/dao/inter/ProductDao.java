package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.Product;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: ProductDao
 * Description: 商品信息Dao
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface ProductDao extends BaseDao<Product, String> {

    public Page<Product> queryPage(Map<String, Object> params);

    public List<Product> queryList(Map<String, Object> params);

    public boolean isDupName(String orgId, String id, String name);

    public boolean isDupCode(String orgId, String id, String code);
}