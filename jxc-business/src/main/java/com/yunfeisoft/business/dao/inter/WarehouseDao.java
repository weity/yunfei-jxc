package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.Warehouse;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: WarehouseDao
 * Description: 仓库信息Dao
 * Author: Jackie liu
 * Date: 2020-08-04
 */
public interface WarehouseDao extends BaseDao<Warehouse, String> {

    public Page<Warehouse> queryPage(Map<String, Object> params);

    public List<Warehouse> queryList(Map<String, Object> params);

    public int modifyDefault(String id, String orgId);

    public Warehouse loadDefault(String orgId);

    public boolean isDupName(String orgId, String id, String name);
}