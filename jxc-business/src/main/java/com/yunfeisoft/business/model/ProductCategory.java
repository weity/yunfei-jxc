package com.yunfeisoft.business.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.applet.base.ServiceModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * ClassName: ProductCategory
 * Description: 商品类别
 *
 * @Author: Jackie liu
 * Date: 2020-07-23
 */
@Entity
@Table(name = "TT_PRODUCT_CATEGORY")
public class ProductCategory extends ServiceModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    @Column
    private String orgId;

    /**
     * 父id
     */
    @Column
    private String parentId;

    /**
     * 路径id
     */
    @Column
    private String idPath;

    /**
     * 编码
     */
    @ExcelProperty("编码")
    @Column
    private String code;

    /**
     * 名称
     */
    @ExcelProperty("名称")
    @Column
    private String name;

    /**
     * 名称路径
     */
    @Column
    private String namePath;

    /**
     * 拼音简码
     */
    @ExcelProperty("拼音简码")
    @Column
    private String pinyinCode;


    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getIdPath() {
        return idPath;
    }

    public void setIdPath(String idPath) {
        this.idPath = idPath;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamePath() {
        return namePath;
    }

    public void setNamePath(String namePath) {
        this.namePath = namePath;
    }

    public String getPinyinCode() {
        return pinyinCode;
    }

    public void setPinyinCode(String pinyinCode) {
        this.pinyinCode = pinyinCode;
    }


}