package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.CodeBuilderDao;
import com.yunfeisoft.business.model.CodeBuilder;
import com.yunfeisoft.business.service.inter.CodeBuilderService;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * ClassName: CodeBuilderServiceImpl
 * Description: 编码生成记录service实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Service("codeBuilderService")
public class CodeBuilderServiceImpl extends BaseServiceImpl<CodeBuilder, String, CodeBuilderDao> implements CodeBuilderService {

    public static final Object LOCK = new Object();

    @Override
    @DataSourceChange(slave = true)
    public Page<CodeBuilder> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public String generateCode(String type, int length, String orgId) {
        synchronized (LOCK) {
            List<CodeBuilder> builderList = getDao().queryByName(type, orgId);
            if (CollectionUtils.isEmpty(builderList)) {
                CodeBuilder builder = new CodeBuilder();
                builder.setName(type);
                builder.setNum(1);
                builder.setOrgId(orgId);
                getDao().insert(builder);
                return String.format("%0" + length + "d", 1);
            }

            CodeBuilder builder = builderList.get(0);
            int num = builder.getNum() + 1;
            builder.setNum(num);
            getDao().update(builder);
            return String.format("%0" + length + "d", num);
        }
    }

    @Override
    public String generateSaleOrderCode(String orgId) {
        String code = generateCode("SALE_ORDER", 5, orgId);
        String date = new DateTime().toString("yyyyMMddHHmmss");
        return new StringBuilder().append("X").append(date).append(code).toString();
    }

    @Override
    public String generatePurchaseOrderCode(String orgId) {
        String code = generateCode("PURCHASE_ORDER", 5, orgId);
        String date = new DateTime().toString("yyyyMMddHHmmss");
        return new StringBuilder().append("C").append(date).append(code).toString();
    }

    @Override
    public String generateAllotOrderCode(String orgId) {
        String code = generateCode("ALLOT_ORDER", 5, orgId);
        String date = new DateTime().toString("yyyyMMddHHmmss");
        return new StringBuilder().append("D").append(date).append(code).toString();
    }

    @Override
    public String generateIndecOrderCode(String orgId) {
        String code = generateCode("INDEC_ORDER", 5, orgId);
        String date = new DateTime().toString("yyyyMMddHHmmss");
        return new StringBuilder().append("S").append(date).append(code).toString();
    }
}