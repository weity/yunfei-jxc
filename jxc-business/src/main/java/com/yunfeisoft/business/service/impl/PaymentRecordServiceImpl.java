package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.PaymentRecordDao;
import com.yunfeisoft.business.dao.inter.PurchaseOrderDao;
import com.yunfeisoft.business.dao.inter.SupplierDao;
import com.yunfeisoft.business.model.PaymentRecord;
import com.yunfeisoft.business.model.PurchaseOrder;
import com.yunfeisoft.business.model.Supplier;
import com.yunfeisoft.business.service.inter.PaymentRecordService;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * ClassName: PaymentRecordServiceImpl
 * Description: 付款信息service实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Service("paymentRecordService")
public class PaymentRecordServiceImpl extends BaseServiceImpl<PaymentRecord, String, PaymentRecordDao> implements PaymentRecordService {

    @Autowired
    private PurchaseOrderDao purchaseOrderDao;
    @Autowired
    private SupplierDao supplierDao;

    @Override
    @DataSourceChange(slave = true)
    public Page<PaymentRecord> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public List<PaymentRecord> queryByPurchaseOrderId(String purchaseOrderId) {
        return getDao().queryByPurchaseOrderId(purchaseOrderId);
    }

    @Override
    public int batchSave2(String orgId, String[] purchaseOrderIds) {
        if (ArrayUtils.isEmpty(purchaseOrderIds)) {
            return 0;
        }
        for (String purchaseOrderId : purchaseOrderIds) {
            PurchaseOrder purchaseOrder = purchaseOrderDao.load(purchaseOrderId);
            if (purchaseOrder == null) {
                continue;
            }
            if (purchaseOrder.getStatus() != PurchaseOrder.PurchaseOrderStatusEnum.STORAGED.getValue()
                    && purchaseOrder.getPayStatus() != PurchaseOrder.PurchaseOrderPayStatusEnum.TO_BE_CLEARED.getValue()) {
                continue;
            }

            PaymentRecord paymentRecord = new PaymentRecord();
            paymentRecord.setOrgId(orgId);
            paymentRecord.setPurchaseOrderId(purchaseOrderId);
            paymentRecord.setSupplierId(purchaseOrder.getSupplierId());
            paymentRecord.setDueAmount(purchaseOrder.getSurplusAmount());
            paymentRecord.setPayAmount(paymentRecord.getDueAmount());
            paymentRecord.setOwedAmount(BigDecimal.ZERO);

            PurchaseOrder order = new PurchaseOrder();
            order.setId(paymentRecord.getPurchaseOrderId());
            order.setPayStatus(PurchaseOrder.PurchaseOrderPayStatusEnum.PAID.getValue());
            order.setPayAmount(purchaseOrder.getTotalAmount());
            purchaseOrderDao.update(order);

            Supplier supplier = supplierDao.load(purchaseOrder.getSupplierId());
            supplier.setId(purchaseOrder.getSupplierId());
            supplier.setBalance(supplier.getBalance().add(paymentRecord.getPayAmount()));
            supplierDao.update(supplier);

            getDao().insert(paymentRecord);
        }
        return purchaseOrderIds.length;
    }

    @Override
    public int save(PaymentRecord paymentRecord) {
        PurchaseOrder purchaseOrder = purchaseOrderDao.load(paymentRecord.getPurchaseOrderId());
        if (purchaseOrder == null) {
            return 0;
        }
        if (purchaseOrder.getStatus() != PurchaseOrder.PurchaseOrderStatusEnum.STORAGED.getValue()
                && purchaseOrder.getPayStatus() != PurchaseOrder.PurchaseOrderPayStatusEnum.TO_BE_CLEARED.getValue()) {
            return 0;
        }
        paymentRecord.setSupplierId(purchaseOrder.getSupplierId());
        paymentRecord.setDueAmount(purchaseOrder.getSurplusAmount());
        paymentRecord.setOwedAmount(paymentRecord.getDueAmount().subtract(paymentRecord.getPayAmount()));

        BigDecimal payAmount = purchaseOrder.getPayAmount().add(paymentRecord.getPayAmount());

        if (paymentRecord.getOwedAmount().compareTo(BigDecimal.ZERO) == 0) {
            PurchaseOrder order = new PurchaseOrder();
            order.setId(paymentRecord.getPurchaseOrderId());
            order.setPayStatus(PurchaseOrder.PurchaseOrderPayStatusEnum.PAID.getValue());
            order.setPayAmount(payAmount);
            purchaseOrderDao.update(order);
        } else {
            PurchaseOrder order = new PurchaseOrder();
            order.setId(paymentRecord.getPurchaseOrderId());
            order.setPayStatus(PurchaseOrder.PurchaseOrderPayStatusEnum.TO_BE_CLEARED.getValue());
            order.setPayAmount(payAmount);
            purchaseOrderDao.update(order);
        }

        Supplier supplier = supplierDao.load(purchaseOrder.getSupplierId());
        supplier.setId(purchaseOrder.getSupplierId());
        supplier.setBalance(supplier.getBalance().add(paymentRecord.getPayAmount()));
        supplierDao.update(supplier);

        return super.save(paymentRecord);
    }
}