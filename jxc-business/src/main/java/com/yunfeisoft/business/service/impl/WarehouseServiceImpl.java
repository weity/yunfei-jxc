package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.yunfeisoft.business.dao.inter.WarehouseDao;
import com.yunfeisoft.business.model.Warehouse;
import com.yunfeisoft.business.service.inter.WarehouseService;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * ClassName: WarehouseServiceImpl
 * Description: 仓库信息service实现
 * Author: Jackie liu
 * Date: 2020-08-04
 */
@Service("warehouseService")
public class WarehouseServiceImpl extends BaseServiceImpl<Warehouse, String, WarehouseDao> implements WarehouseService {

    @Override
    @DataSourceChange(slave = true)
    public Page<Warehouse> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public List<Warehouse> queryList(Map<String, Object> params) {
        return getDao().queryList(params);
    }

    @Override
    public int modifyDefault(String id, String orgId) {
        return getDao().modifyDefault(id, orgId);
    }

    @Override
    public Warehouse loadDefault(String orgId) {
        return getDao().loadDefault(orgId);
    }

    @Override
    public boolean isDupName(String orgId, String id, String name) {
        return getDao().isDupName(orgId, id, name);
    }
}