package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.Product;

import java.util.List;
import java.util.Map;

/**
 * ClassName: ProductService
 * Description: 商品信息service接口
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface ProductService extends BaseService<Product, String> {

    public Page<Product> queryPage(Map<String, Object> params);

    public List<Product> queryList(Map<String, Object> params);

    public boolean isDupName(String orgId, String id, String name);

    public boolean isDupCode(String orgId, String id, String code);
}