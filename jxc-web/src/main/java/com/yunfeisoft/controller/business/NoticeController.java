package com.yunfeisoft.controller.business;

import com.applet.base.BaseController;
import com.applet.utils.Page;
import com.applet.utils.Response;
import com.applet.utils.ResponseUtils;
import com.applet.utils.Validator;
import com.yunfeisoft.business.model.Notice;
import com.yunfeisoft.business.service.inter.NoticeService;
import com.yunfeisoft.model.User;
import com.yunfeisoft.service.inter.DataService;
import com.yunfeisoft.utils.ApiUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: NoticeController
 * Description: 公告信息Controller
 * Author: Jackie liu
 * Date: 2020-08-23
 */
@Controller
public class NoticeController extends BaseController {

    @Autowired
    private NoticeService noticeService;
    @Autowired
    private DataService dataService;

    /**
     * 添加公告信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/notice/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(Notice record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "title", "标题为空");
        validator.required(request, "type", "栏目为空");
        validator.required(request, "content", "内容为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        User user = ApiUtils.getLoginUser();
        record.setOrgId(user.getOrgId());

        noticeService.save(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 修改公告信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/notice/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(Notice record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "id", "参数错误");
        validator.required(request, "title", "标题为空");
        validator.required(request, "type", "栏目为空");
        validator.required(request, "content", "内容为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }
        noticeService.modify(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 查询公告信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/notice/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        Notice record = noticeService.load(id);
        if (record != null) {
            String content = dataService.loadByRefId(record.getId());
            record.setContent(content);
        }
        return ResponseUtils.success(record);
    }

    /**
     * 分页查询公告信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/notice/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String title = ServletRequestUtils.getStringParameter(request, "title", null);
        String type = ServletRequestUtils.getStringParameter(request, "type", null);

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("title", title);
        params.put("type", type);

        Page<Notice> page = noticeService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 批量删除公告信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/notice/delete", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response delete(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isBlank(ids)) {
            return ResponseUtils.warn("参数错误");
        }
        noticeService.remove(ids.split(","));
        return ResponseUtils.success("删除成功");
    }
}
