<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>编辑供应商信息</title>
	<#include "/common/vue_resource.ftl">
	<script type="text/javascript" src="${params.contextPath}/js/Convert_Pinyin.js"></script>
</head>
<body>
<div id="app" v-cloak>
	<div class="ui-form">
		<form class="layui-form" @submit.prevent="submitForm()" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">名称<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="name" placeholder="请输入公司名称或者联系人姓名" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">联系人</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.linkman" placeholder="请输入联系人" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">传真</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.fax" placeholder="请输入传真" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">常用电话</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.phone" placeholder="请输入常用电话" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">备用电话</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.standbyPhone" placeholder="请输入备用电话" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">地址</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.address" placeholder="请输入地址" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">QQ号</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.qq" placeholder="请输入QQ号" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">微信</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.wechat" placeholder="请输入微信" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">旺旺</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.wangwang" placeholder="请输入旺旺" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">Email</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.email" placeholder="请输入Email" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">网址</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.website" placeholder="请输入网址" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">银行账户</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.bankAccount" placeholder="请输入银行账户" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">银行户名</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.bankName" placeholder="请输入银行户名" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">账号/卡号</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.bankNo" placeholder="请输入账号/卡号" class="layui-input"/>
				</div>
			</div>
			<#--<div class="layui-form-item">
				<label class="layui-form-label">预付余额</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.balance" placeholder="请输入预付余额" class="layui-input"/>
				</div>
			</div>-->
			<div class="layui-form-item">
				<label class="layui-form-label">拼音简码</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.pinyinCode" placeholder="请输入拼音简码" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">备注</label>
				<div class="layui-input-block">
					<textarea v-model="record.remark" placeholder="请输入备注" class="layui-textarea"></textarea>
				</div>
			</div>

			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="submit" value="保存" class="layui-btn" />
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	var app = new Vue({
		el: '#app',
		data: {
			showTypes: false,
			record : {
				id:'${params.id!}',
				code:'',
				name:'',
				linkman:'',
				fax:'',
				phone:'',
				standbyPhone:'',
				address:'',
				qq:'',
				wechat:'',
				wangwang:'',
				email:'',
				website:'',
				bankAccount:'',
				bankName:'',
				bankNo:'',
				remark:'',
				//balance:'',
				pinyinCode:'',
			},
			name: ''
		},
		mounted: function () {
			this.init();
			this.loadData();
		},
		watch: {
			'name': function (newVal, oldVal) {
				this.record.name = newVal || "";
				if (!newVal) {
					this.record.pinyinCode = "";
					return false;
				}
				this.record.pinyinCode = pinyin.getCamelChars(newVal);
			}
		},
		methods: {
			init:function(){
				var that = this;
				/*laydate.render({elem: '#beginDate', type:'datetime', done:function (value) {
						that.record.beginDate = value;
					}});*/

			},
			loadData: function () {
				if (!'${params.id!}') {
					return;
				}
				var that = this;
				$.http.post('${params.contextPath}/web/supplier/query.json', {id: '${params.id!}'}).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var item = data.data;
					for (var key in  that.record) {
						that.record[key] = item[key];
					}
					that.name = item.name || "";
				});
			},
			submitForm: function () {
				$.http.post('${params.contextPath}/web/supplier/<#if (params.id)??>modify<#else>save</#if>.json', this.record).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var alt = layer.alert(data.message || "操作成功", function () {
						parent.app.loadData();
						parent.layer.closeAll();
						layer.close(alt);
					});
				});
			},
		}
	});
</script>
</body>

</html>
