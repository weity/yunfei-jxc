<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>编辑组织机构信息</title>
    <link rel="stylesheet" type="text/css" href="${params.contextPath}/common/ztree/css/zTreeStyle/zTreeStyle.css"/>
    <#include "/common/resource.ftl">
    <script type="text/javascript" src="${params.contextPath}/common/ztree/js/jquery.ztree.all.min.js"></script>
    <script type="text/javascript">
        $(function () {
            <#if (params.id)??>
                $.ajaxRequest({
                    url:'${params.contextPath}/web/organization/query.json',
                    data:{id:"${params.id}"},
                    success:function (data) {
                        if (!data.success) {
                            layer.msg(data.message);
                            return;
                        }
                        var record = data.data;
                        for (var key in record) {
                            $("[name='"+key+"']").val(record[key]);
                        }
                    }
                });
            </#if>
        });
    </script>
</head>
<body>
    <div class="ui-form" style="background:#fff;margin:20px;">
        <form class="layui-form ajax-form" action="${params.contextPath}/web/organization/modifyCompany.json" method="post">
            <input type="hidden" name="id" value=""/>
            <div class="layui-form-item">
                <label class="layui-form-label">公司名称<span class="ui-required">*</span></label>
                <div class="layui-input-block">
                    <input type="text" name="name" value="${(user.organization.name)!}" placeholder="请输入名称" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">联系电话</label>
                <div class="layui-input-block">
                    <input type="text" name="telephone" value="${(user.organization.telephone)!}" placeholder="请输入联系电话" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">备注</label>
                <div class="layui-input-block">
                    <textarea name="remark" value="${(user.organization.remark)!}" class="layui-textarea" placeholder="请输入备注" maxlength="200"></textarea>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <input type="submit" value="保存" class="layui-btn"/>
                    <button type="button" class="layui-btn layui-btn-danger" @click="top.showNotice('company_info')">使用技巧</button>
                </div>
            </div>
        </form>
    </div>
</body>

</html>
